# FAN GALORE

This is the application codebase for [the FAN GALORE web shop](https://fangalore.com).

## Get started

```
$ npm i
$ pip install -r requirements.txt

$ export DATABASE_URL=postgresql://postgres@localhost/fangalore

$ export BABEL_BIN=`pwd`/node_modules/.bin/babel
$ export POSTCSS_BIN=`pwd`/node_modules/.bin/postcss

$ export SECRET_KEY=SecureUniqueSequenceOfCharacters

$ flask assets build
$ flask db create

$ gunicorn app
```
