module.exports = {

    comments: false,

    plugins: [

        "@babel/plugin-external-helpers",
        "@babel/plugin-syntax-dynamic-import"
    ],
    
    presets: [

        ["@babel/env"],
        ["minify", { "mangle": false }]
    ]
};
