from sys import exc_info
from traceback import format_tb

from flask import make_response, render_template, request

from . import _, app
from .api import api


@app.errorhandler(Exception)
def error(exception):

    data = {'name': 'server error'}

    if app.config['DEBUG']:

        data = _({
            'name': exc_info()[0].__name__,
            'exception': str(exc_info()[1]),
            'traceback': format_tb(exc_info()[2])
        })

    if api.properties.root in request.path:

        if app.config['DEBUG']:

            data.traceback = [tb.strip().replace('"', "'").split(
                '\n') for tb in format_tb(exc_info()[2])]

            for i, tb in enumerate(data.traceback):
                data.traceback[i] = [line.strip() for line in tb]

        return make_response({'error': data}, 500)

    return make_response(render_template('error.haml', **data), 500)
