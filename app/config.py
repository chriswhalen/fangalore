from os import environ
from os.path import join

from werkzeug.middleware.proxy_fix import ProxyFix

from yaml import safe_load as load

from . import _, app


env = None
site = None


try:
    with open(join(app.root_path, 'env.yaml')) as file:

        env = _(load(file.read()))

except OSError:
    pass

try:
    with open(join(app.root_path, 'site.yaml')) as file:

        site = _(load(file.read()))

except OSError:
    pass


app.config['PROXIES'] = environ.get('PROXIES', 0)

app.config['SECURITY_LOGIN_USER_TEMPLATE'] = 'login.haml'
app.config['SECURITY_LOGIN_URL'] = '/_/login'

app.config['SECURITY_PASSWORD_SALT'] = app.config['SECRET_KEY']

app.config['SQLALCHEMY_DATABASE_URI'] = environ.get('SQLALCHEMY_DATABASE_URI',
                                                    environ.get('DATABASE_URL',
                                                                None))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


if env:
    app_env = {}

    for key in env:
        app_env[key.upper()] = env[key]

    env.update(**app_env)
    app.config.update(**app_env)

if app.config['PROXIES']:
    app.wsgi_app = ProxyFix(app.wsgi_app, num_proxies=app.config['PROXIES'])

if app.config['DATABASE_URL'] and not app.config['SQLALCHEMY_DATABASE_URI']:
    app.config['SQLALCHEMY_DATABASE_URI'] = app.config['DATABASE_URL']
