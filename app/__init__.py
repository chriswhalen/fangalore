from os.path import join, realpath, split

from flask import Flask

from flask_assets import Environment

from flask_marshmallow import Marshmallow

from flask_migrate import Migrate

from flask_sqlalchemy import SQLAlchemy

from munch import munchify as _

from werkzeug import ImmutableDict


class FanGalore(Flask):


    jinja_options = ImmutableDict(extensions=[

        'jinja2.ext.autoescape',
        'jinja2.ext.with_',
        'hamlish_jinja.HamlishExtension'
    ])


    def __init__(self, name):

        super().__init__(name)

        self.jinja_env.hamlish_enable_div_shortcut = True
        self.jinja_env.hamlish_mode = 'indented'

        self.jinja_env.variable_start_string = '{|'
        self.jinja_env.variable_end_string = '|}'

        self.root_path = join('/',
                              *split(realpath(__file__))[0].split('/')[:-1])


app = application = FanGalore(__name__)


from . import config


assets = Environment(app)
db = SQLAlchemy(app)
mm = Marshmallow(app)
migrate = Migrate(app, db)

assets.auto_build = False
assets.cache = False
assets.directory = app.root_path
assets.load_path = [app.root_path]
assets.manifest = False
assets.url = '/'

jinja = app.jinja_env
site = config.site


from . import filters, api, components, errors, static, web


app.register_blueprint(api.api, url_prefix=api.api.properties.root[:-1])

app.register_blueprint(components.components, url_prefix='/_/')

app.register_blueprint(web.web)


from . import models


# flake8: noqa
