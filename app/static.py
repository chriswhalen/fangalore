from os.path import join

from flask_assets import Bundle

from webassets.exceptions import BuildError, BundleError, FilterError

from . import assets


try:
    assets.register('vendor_css', Bundle(

        join('styles', 'vendor', '*.css'),
        filters='postcss',
        output=join('static', 'vendor.css')
    ))
except (BuildError, BundleError, FilterError):
    pass


try:
    assets.register('app_css', Bundle(

        join('styles', 'app.sss'),
        depends=(join('styles', '*.sss')),
        filters='postcss',
        output=join('static', 'app.css')
    ))
except (BuildError, BundleError, FilterError):
    pass


try:
    assets.register('vendor_js', Bundle(

        join('scripts', 'vendor', '*.js'),
        filters='rjsmin',
        output=join('static', 'vendor.js')
    ))
except (BuildError, BundleError, FilterError):
    pass


try:
    assets.register('app_js', Bundle(

        join('scripts', 'app.js'),
        depends=(join('scripts', '*.js')),
        filters=('require', 'babel'),
        output=join('static', 'app.js')
    ))
except (BuildError, BundleError, FilterError):
    pass
