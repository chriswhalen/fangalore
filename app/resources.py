from re import sub

from flask_restful import Resource as RestResource

from sqlalchemy.exc import DataError
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

from . import _, db, mm
from .api import api, error, rest


def resource(this):

    class Schema(mm.ModelSchema):

        class Meta:

            model = this
            exclude = []
            only = []

            try:
                exclude = this.Meta.exclude

            except AttributeError:
                pass

            try:
                only = this.Meta.only

            except AttributeError:
                pass

    class Resource(RestResource):

        class Meta:

            model = this
            allowed = _({
                'get': all,
                'post': None,
                'put': None,
                'patch': None,
                'delete': None
            })

            try:
                for method in this.Meta.allowed:
                    allowed[method] = this.Meta.allowed[method]

            except AttributeError:
                pass

        def query(self, method, **params):

            if self.Meta.allowed[method] is all:
                return self.Meta.model.query

            if self.Meta.allowed[method] is None:
                return self.Meta.model.query.limit(0).from_self()

            return self.Meta.allowed[method](**params).from_self()

        def get(self, **params):

            try:
                query = self.query('get', **params)

                if 'id' in params:
                    return self.Meta.model.one.dump(
                        query.filter_by(**params).one())

                return self.Meta.model.many.dump(query.all())

            except NoResultFound:
                return error(404)

            except (DataError, MultipleResultsFound):
                return error(400)

    def __repr__(self):

        return '<%s %i>' % (this.__class__.name, self.id)

    def delete(self):

        self.session.delete(self)
        self.session.commit()

    def save(self):

        if not self.id:
            self.session.add(self)

        self.session.commit()

    Schema.__name__ = '%sSchema' % (this.__name__,)
    Resource.__name__ = '%sResource' % (this.__name__,)

    this.schema = Schema
    this.resource = Resource

    this.session = db.create_session(options={'bind': [this]})

    this.one = Schema(many=False)
    this.many = Schema(many=True)

    this.delete = delete
    this.save = save

    try:
        name = this.Meta.endpoint

    except AttributeError:
        name = sub(r'([A-Z])', r'_\1', this.__name__)
        name = '%ss' % (name[1:].lower(),)

    try:
        rest.add_resource(Resource,
                          '/%s/' % (name,),
                          '/%s/<string:id>' % (name,))

        api.properties.endpoints[this.__name__] = '%s%s/' % (
                                                    api.properties.root, name)

    except AssertionError:
        pass

    return this
