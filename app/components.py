from os import listdir
from os.path import exists, isdir, join

from flask import Blueprint, render_template

from flask_assets import Bundle

from jinja2.exceptions import TemplateNotFound

from webassets.exceptions import BuildError, BundleError, FilterError

from . import assets


components = Blueprint('components', __name__, template_folder='../components')


names = [c if isdir(join('components', c)) else None
         for c in listdir('components')]


for name in names:

    if not name:
        continue

    bundles = []
    path = join('components', name, '')

    if exists('%s%s' % (path, 'template.haml')):

        bundles.append(Bundle('%s%s' % (path, 'template.haml'),
                              filters=('hamlish')))

    if exists('%s%s' % (path, 'style.sss')):

        bundles.append(Bundle('%s%s' % (path, 'style.sss'),
                              filters=('postcss_fix',
                                       'postcss',
                                       'wrap_style')))

    if exists('%s%s' % (path, 'script.js')):

        bundles.append(Bundle('%s%s' % (path, 'script.js'),
                              filters=('require',
                                       'babel',
                                       'wrap_script')))

    try:
        assets.register(name,
                        *bundles,
                        filters='strip',
                        output=join('components',
                                    '%s.html' % (name,)))

    except (BuildError, BundleError, FilterError):
        pass


@components.route('<path:path>')
def component(path):

    try:
        return render_template('%s.html' % (path,))

    except TemplateNotFound:
        return '<!-- %s not found -->' % (path,)
