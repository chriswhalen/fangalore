from datetime import datetime

from flask import url_for

from . import _, site


def asset(name):

    try:
        return url_for('static', filename=name)

    except RuntimeError:
        return '/static/%s' % (name,)


def context(key=None):

    context = _({

        'asset': asset,
        'now': datetime.now(),
        'site': site,
        'title': lambda text=None: ('%s | %s' % (text, site.name)
                                    if text else site.name)
    })

    return context[key] if key in context else None if key else context
