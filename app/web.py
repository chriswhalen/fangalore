from flask import Blueprint, redirect, render_template

from .api import api
from .context import context


web = Blueprint('web', __name__)
web.context_processor(context)


@web.route('/favicon<name>')
def favicon(name):

    return redirect(context('asset')('images/favicon%s' % (name,)))


@web.route('/')
@web.route('/<path:path>')
def index(**kwargs):

    if 'path' in kwargs:

        if kwargs['path'] == 'api':
            return redirect(api.properties.root)

        if kwargs['path'][:4] == 'api/':
            return redirect(''.join([api.properties.root,
                                    kwargs['path'].replace('api/', '')]))

    return render_template('index.haml')
