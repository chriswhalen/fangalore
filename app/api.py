from flask import Blueprint, make_response

from flask_restful import Api

from . import _, site


api = Blueprint('api', __name__)
rest = Api(api)


api.properties = _({
    'endpoints': {},
    'root': '/api/v%i/' % (site.api.version,),
    'version': site.api.version,
    'updated_at': site.api.updated_at
})


def error(code=500):

    errors = {
        400: {'code': 400, 'message': 'bad request'},
        404: {'code': 404, 'message': 'not found'},
        500: {'code': 500, 'message': 'internal server error'}
    }

    if code not in errors:
        code = 500

    return make_response({'error': errors[code]}, code)


@api.route('/')
def index():

    return api.properties


@api.route('/<path:path>')
def not_found(**kwargs):

    return error(404)
