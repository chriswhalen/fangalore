from os.path import join, split
from re import sub

from webassets.filter import Filter, register_filter

from . import jinja
from .context import context


@register_filter
class Hamlish(Filter):

    name = 'hamlish'

    def input(self, _in, _out, **kwargs):

        _out.write(jinja.hamlish_from_string(_in.read(),
                                             globals=context()).render())


@register_filter
class PostcssFix(Filter):

    name = 'postcss_fix'

    def input(self, _in, _out, **kwargs):

        style = _in.read()

        if not len(style.strip()):
            style = '/* */'

        _out.write(style)


@register_filter
class Require(Filter):

    name = 'require'

    def require(self, match):

        path = list(split(match.group(1)))
        path[-1] = '%s.js' % (path[-1],)

        prefix_path = [p for p in path]
        prefix_path[-1] = '_%s' % (prefix_path[-1],)

        try:
            return open(join(self.path, *prefix_path), 'r').read()

        except FileNotFoundError:
            return open(join(self.path, *path), 'r').read()

    def input(self, _in, _out, **kwargs):

        self.path = split(kwargs['source_path'])[0]
        for line in _in.readlines():

            _out.write(sub(r'require\(\s*["\'](.*)["\']\s*\)\s*;?',
                           self.require,
                           line))


@register_filter
class Strip(Filter):

    name = 'strip'

    def output(self, _in, _out, **kwargs):

        for line in _in.readlines():

            if len(line.strip()):
                _out.write(line)


@register_filter
class WrapScript(Filter):

    name = 'wrap_script'

    def output(self, _in, _out, **kwargs):

        script = _in.read()

        if len(script.strip()) > len('"use strict";'):
            _out.write('<script async>\n%s\n</script>' % (script,))


@register_filter
class WrapStyle(Filter):

    name = 'wrap_style'

    def output(self, _in, _out, **kwargs):

        style = _in.read()

        if len(style.strip()):
            _out.write('<style>\n%s\n</style>' % (style,))
