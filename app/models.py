from flask_security import \
    RoleMixin, SQLAlchemyUserDatastore, Security, UserMixin

from . import app, db
from .resources import resource


@resource
class Role(db.Model, RoleMixin):

    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.Text, unique=True)
    description = db.Column(db.Text)


UserRole = db.Table(

    'user_role',

    db.Column('role_id', db.Integer,
              db.ForeignKey('role.id'), primary_key=True),
    db.Column('user_id', db.Integer,
              db.ForeignKey('user.id'), primary_key=True)
)


@resource
class User(db.Model, UserMixin):

    class Meta:
        exclude = ['password']

    id = db.Column(db.Integer, primary_key=True)

    email = db.Column(db.Text, unique=True, nullable=False)
    password = db.Column(db.Text)
    active = db.Column(db.Boolean)
    confirmed_at = db.Column(db.DateTime)

    roles = db.relationship('Role', secondary=UserRole,
                            backref=db.backref('users', lazy=True))


UserStore = SQLAlchemyUserDatastore(db, User, Role)

app.security = Security(app, UserStore, register_blueprint=False)


@resource
class MediaType(db.Model):

    name = db.Column(db.Text, primary_key=True)


@resource
class Media(db.Model):

    class Meta:
        endpoint = 'media'

    id = db.Column(db.Integer, primary_key=True)

    title = db.Column(db.Text)
    description = db.Column(db.Text)
    path = db.Column(db.Text)

    type = db.Column(db.Text, db.ForeignKey('media_type.name'))


@resource
class Tag(db.Model):

    name = db.Column(db.Text, primary_key=True)


@resource
class Category(db.Model):

    class Meta:
        endpoint = 'categories'

    name = db.Column(db.Text, primary_key=True)


@resource
class Review(db.Model):

    id = db.Column(db.Integer, primary_key=True)

    title = db.Column(db.Text)
    description = db.Column(db.Text)
    score = db.Column(db.Integer)
    quality = db.Column(db.Integer)

    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


ProductTag = db.Table(

    'product_tag',

    db.Column('product_id', db.Integer,
              db.ForeignKey('product.id'), primary_key=True),
    db.Column('tag', db.Text,
              db.ForeignKey('tag.name'), primary_key=True)
)


ProductMedia = db.Table(

    'product_media',

    db.Column('product_id', db.Integer,
              db.ForeignKey('product.id'), primary_key=True),
    db.Column('media_id', db.Integer,
              db.ForeignKey('media.id'), primary_key=True)
)


@resource
class Product(db.Model):

    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.Text, unique=True, nullable=False)
    title = db.Column(db.Text)
    description = db.Column(db.Text)
    base_price = db.Column(db.Integer)
    sale_price = db.Column(db.Integer)
    properties = db.Column(db.JSON)
    active = db.Column(db.Boolean)

    category = db.Column(db.Text, db.ForeignKey('category.name'))

    reviews = db.relationship('Review', backref='product', lazy=True)

    tags = db.relationship('Tag', secondary=ProductTag,
                           backref=db.backref('products', lazy=True))

    media = db.relationship('Media', secondary=ProductMedia,
                            backref=db.backref('products', lazy=True))
