const Data = {}

const GetSet = {
    get: (target, name)=>{ if (target.get(name)){ return target.get(name) } return target[name] },
    set: (target, name, value)=>{ if (target.get(name)){ return target.set(name, value) } return target[name] = value }
}

Backbone.sync =(action, resource, params)=>{

    console.log(action, resource, params)

    const actions = {

        read: {}
    }

    if (_.isUndefined(resource)) return resource

    let url = resource.url
    if (_.isFunction(resource.url)) url = resource.url()

    return fetch(url, actions[action]).
        then((response)=> response.json().then((body)=>{
            resource.set(body)
        }))
}


fetch('/api').then((response)=> response.json().then((api)=>{

    _(api.endpoints).each((path, name)=>{

        let Collection = Backbone.Collection.extend({

            model: (attributes, params)=>{

                let model = new Backbone.Model(attributes, params)
                return new Proxy(model, GetSet)
            },

            url: path
        })

        let collection = new Collection
        collection.fetch()
        
        Data[name] = new Proxy(collection, GetSet)
    })
}))

/* global _, Backbone */
