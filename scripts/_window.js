$(window).scroll(()=>{

    if ($(window).scrollTop() > 24) $('body').addClass('is-scrolled')
    else $('body').removeClass('is-scrolled')

}).scroll()


$(window).resize(()=>{

    $('body').removeClass('small mobile tablet desktop widescreen fullhd')
    if ($(window).width() < 480) $('body').addClass('small')
    if ($(window).width() < 769) $('body').addClass('mobile')
    else if ($(window).width() < 1024) $('body').addClass('tablet')
    else if ($(window).width() < 1216) $('body').addClass('desktop')
    else if ($(window).width() < 1408) $('body').addClass('widescreen')
    else $('body').addClass('fullhd')

}).resize()


Backbone.history.start({pushState: true, silent: true})


/* global $, Backbone */
