const Router = Backbone.Router.extend({

    routes: {
        cart:    'cart',
        '':      'index'
    },

    preinitialize: ()=>{
        render('window', 'body').then(()=> Page.reload())
    },

    reload: ()=>{
        let path = location.pathname.substring(1)
        if (!_.isUndefined(Page.routes[path])) Page[Page.routes[path]]()
    },

    index: ()=>{
        render('home', '#content')
    },

    cart: ()=>{
        render('cart', '#content')
    }

})

const Page = new Router()


/* global _, Backbone, render */
